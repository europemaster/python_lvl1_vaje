# Instructions

# Create a program that represents a football bet.
# The program needs to have a list of football matches, where for every match user should input
# the end result. There needs to be at least 5 matches. Example of input:
# (programs output) Liverpool : CSKA ?
# (user's input) 1-0
# (programs output) Bate Borisov : Partizan  ?
#  ....

# After user inputs all the results, program waits for 10 seconds and prints the result of matches.
# Results are totally random, you can say that max. goals by one team is 5.
# After results are displayed, program should output, how well were the bets.
# For every correctly guessed match, user gets 10€, for every incorrect user loses 1€
# Example of output:
# Actual game: Liverpool : CSKA 3-0.
# Your bet: 1-0
# You lose 1€
# ....
# --------------
# You earned -5€.
